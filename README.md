# Ansible F5 Upgrade 

The project aims to seamlessly upgrade F5s (Both standalone and HA pairs) with Ansible.
Fork/clone, use at your own risk, this play reboots stuff and things.

## How it works - simple

### Architecture

![Design](readmefiles/arch_image.png "Deployment Design")

### Procedures

1. The pre_tasks will check if A - the unit is HA or standalone (ha_info) and B - the HA state of host (ha_state); note that standalone will always be Active
2. The bigip_download_image role will download the image + hash from external file repository using curl then compares hash between image and md5 file - the playbook has been tested on Azure blob SAS.
 BIG CAVEAT!!! - Ensure you escape characters which Ansible hates - i.e. for SAS "&" needed to be escaped (as in "\&")
3. bigip_ucs_backup will take UCS backup and save to local system (aimed for Linux hosts)
4. bigip_license_reactivate_standby will reactivate license via the F5 license server on the standby unit (skips if standalone)
5. bigip_software_activation_standby will install and activate software on specified partition on the standby unit (skips if standalone)
6. bigip_traffic_failover will initiate failover of traffic from active unit (will only run on HA pairs)
7. bigip_license_reactivate_active will reactivate license via the F5 license server on the active unit
8. bigip_software_activation_active will install and activate software on specified partition on the active unit
9. bigip_cleanup_image will remove the downloaded image and md5 hash file off the system

## How to use

Simply clone, install requirements, edit inventory, listed vars and run.

```bash
git clone https://gitlab.com/knagatori/ansible-f5-upgrade.git

cd ansible-f5-upgrade

ansible-galaxy install -r requirements.yml

# do some edits

ansible-playbook -i <inventory> bigip_upgrade.yml
```

When running the main upgrade playbook, you will need to supply the username and password for the F5 TMUI.
Everything else will happen without user intervetion

### Mandatory vars
Below assumes that all hosts will be running same version and will be using same external storage - fork to your usecase.

#### Specified on inventory
**private_ip** - IP of the F5 - specify in inventory

**jumphost** - if you are using bastion for delegation, put in jumphost, otherwise put in localhost

**image** - full image name (including .iso)

**azure_sas_hash** - Azure SAS of image hash file

**azure_sas_image** - Azure SAS of ISO image file

**!!! Very important, consider the characters that need to be escaped - recommend unit testing esp if you are using other storage types**

**chg** - Change number - used to name UCS backup file

**backup_loc** - dir which UCS will be stored

####  Specified on playbook run
**ansible_user** - TMUI username

**ansible_password** - TMUI password

####  Group/Host Vars
**upstream_proxy** - (Optional) If your F5 needs proxy to get internet access - specify the IP address

**upstream_proxy_port** - (Optional) If you are using above proxym specify the port

**volume** - Partition where new software will be installed and booted (HDx.x)

## External ref

Japanese - https://zenn.dev/kurokumakun/articles/ef74d9e7331830

